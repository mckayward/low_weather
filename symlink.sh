#!/bin/bash

original_file=low_weather.rb
new_file=.low_weather_linked_file.rb
ruby=`which ruby`
file_path="`pwd`/$new_file"

echo -n "#!" > $new_file          # add shebang
echo $ruby >> $new_file           # add ruby path
cat $original_file >> $new_file   # add contents of low_weather.rb

chmod +x $new_file                # create excecutable file

sudo ln -s $file_path /usr/bin/lw # create symbolic link
