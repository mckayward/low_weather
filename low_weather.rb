require 'open-uri'
require 'nokogiri'
require 'nexmo' if ARGV[1]

CITY         = ARGV[0]
PHONE_NUMBER = ARGV[1]
WEATHER_SITE = "http://www.wpc.ncep.noaa.gov/discussions/hpcdiscussions.php?disc=nathilo"

def get_site
  # Checks internet connection and gets HTML
  attempts = 1

  begin
    Nokogiri::HTML(open(WEATHER_SITE))
  rescue
    if attempts <= 10
      puts "No connection. Retrying..."
      attempts += 1
      sleep(60 * 20)
      retry
    end
    abort("A connection could not be established. Program Terminated")
  end
end

def send_text(message)
  nexmo = Nexmo::Client.new
  nexmo.send_message(from: ENV['NEXMO_NUMBER'], to: PHONE_NUMBER, text: message)
end

def main
  page = get_site

  text = page.css('div#printarea').text

  # Parse it
  body = text.match(/(?<=Low\sTemperature\sfor\s).*(?=\n\n)/m)
  body_array = body.to_s.split("\n")

  date = body_array.shift # Get date from the first element
  body_array.shift        # Remove subtext that was parsed into the array as the second element

  body_array.each do |city_temp|
    message = "On #{date}, the lowest temperature in the country was recorded as #{city_temp}"

    if city_temp.match("#{CITY}")
      send_text(message) if PHONE_NUMBER
    end

    puts message
  end
end

main

