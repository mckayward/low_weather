low_weather
===========

low_weather will send a text message to a user-specified number when a specified
location was recorded as having the lowest temperature in the lower 48 that day.
When run on a daily basis with cron, the program allows daily monitoring of the
coldest temperature recorded in the lower 48.


How to use low_weather
----------------------

###Command Line Utility
After cloning the repository, running `./symlink.sh` will create a symbolic link
on your computer that will allow you to check the day's low weather by typing
`lw` into the command line. Running `./symlink.sh` will create a dotfile in the
project's directory called `.low_weather_linked_file.rb`. This is the file that your
computer will run with the `lw` command.

###Sending a text about the day's low weather
low_weather takes two command-line arguments, a location name that the user
desires to trigger a notification, and a phone number to which the text
message notification should be sent. For example:
```ruby low_weather.rb "Minneapolis, MN" "12223334444"```
would send a text message
to the phone number (222)-333-4444 if the lowest temperature recorded in the
lower 48 were Minneapolis, MN. Note that the phone number must include the
country code and the area code, and must have no spaces or other characters
other than numbers

Note that a just the state code could be used:
```ruby low_weather.rb "MN" "12223334444"```
would send a text to the number
if any city in the state of Minnesota were to have the lowest temperature for
that day.

A city name alone could also be used:
```ruby low_weather.rb "Portland" "12223334444"```
would trigger a text message for both Portland, OR and Portland, ME.

low_weather becomes convenient when it is run using cron on a daily basis.
You should research how to schedule a cron job if you aren't familiar with the
process. But here is a quick overview of how to set up the process.

Open your list of cron jobs by typing `crontab -e` into the command line. Insert
this line into the file to schedule the job and include your city and phone
number:
```30 18 * * * ruby ~/path/to/low_weather.rb <Name of place to be tracked> <phone number>```
for example:
```30 18 * * * ruby ~/projects/ruby/low_weather/low_weather.rb "minneapolis, mn" "12345678910"```

Also note that the program utilizes the open-uri, nokogiri, and nexmo gems. If you
are missing any one of these gems, install it by typing `gem install <name of gem>`

If you would like to use the program, you will need your own Nexmo account to send the
text messages. I have found Nexmo to be easy to use and affordable, and I find myself
using it a fair amount for different projects, so if you don't have an account you may
want to consider starting one. Once you have a Nexmo account, you will need to set your
Nexmo key,secret, and phone number as environment variables called `NEXMO_API_KEY`,
`NEXMO_API_SECRET`, and `NEXMO_NUMBER` respectively.

###Checking the day's low weather
To just see what location in the country had the lowest weather, just run
```ruby low_weather.rb```
This will print out the location with the lowest weather in the country for that day.
